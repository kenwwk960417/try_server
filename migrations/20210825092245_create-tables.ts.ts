import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable("staff",(table)=>{
        table.increments();//id
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.timestamps(false,true);
    });

    await knex.schema.createTable("memos",(table)=>{
        table.increments();
        table.text("content").notNullable();
        table.string("image");
        table.timestamps(false,true);
    });

    await knex.schema.createTable("likes",(table)=>{
        table.increments();
        table.integer("user_id").unsigned();
        table.foreign("user_id").references("staff.id");
        table.timestamps(false,true);
        table.integer("memo_id").unsigned();
        table.foreign("memo_id").references("memos.id");
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("likes");
    await knex.schema.dropTableIfExists("memos");
    await knex.schema.dropTableIfExists("staff");
}
