-- Drop table
-- DROP TABLE public.memos;
CREATE TABLE memos (
    id serial NOT NULL PRIMARY KEY,
    "content" text NULL,
    image varchar(255) NULL,
    created_at timestamp NULL,
    updated_at timestamp NULL,
    CONSTRAINT memos_pkey PRIMARY KEY (id)
);
-- Drop table
-- DROP TABLE public.staff;
CREATE TABLE staff (
    id serial NOT NULL PRIMARY KEY,
    username varchar(255) NULL,
    "password" varchar(255) NULL,
    created_at timestamp NULL,
    updated_at timestamp NULL,
    CONSTRAINT staff_pkey PRIMARY KEY (id)
);
CREATE TABLE likes (
    id SERIAL PRIMARY KEY,
    user_id integer,
    created_at timestamp NULL,
    updated_at timestamp NULL,
    FOREIGN KEY (user_id) REFERENCES staff(id),
    memo_id integer,
    FOREIGN KEY (memo_id) REFERENCEs memos(id)
);