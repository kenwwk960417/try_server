async function getUser() {
    const res = await fetch("/user");
    const data = await res.json();
    console.log(data);
    document.querySelector("#admin-name").innerHTML = data.user.username || data.user.email;
}

async function createUser(e) {
    e.preventDefault();
    const form = e.target;
    const userObj = {
        username: form.username.value,
        password: form.password.value,
    };

    const res = await fetch(`/register`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(userObj),
    });
    const result = await res.json();
    document.querySelector("#create-user-res").innerHTML = result.msg;
    form.reset();
}

async function logout() {
    await fetch("/logout");
    window.location = "/";
}
async function removeAllMemo() {
    const res = await fetch("/memos", {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
    });
    const result = await res.json();
    document.querySelector("#clear-memo-res").innerHTML = result.msg;
}

document.querySelector("form#create-user").addEventListener("submit", createUser);

getUser();
