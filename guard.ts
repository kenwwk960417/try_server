import { Request, Response, NextFunction } from "express";
import { logger } from "./logger";
export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
    if (req.session && req.session["user"]) {
        next();
    } else {
        logger.info("User login first!");
        res.redirect("/");
    }
};
