import {Knex} from 'knex';
import {hashPassword,checkPassword} from '../hash';

export class UserService{
    constructor(private knex:Knex){};
    register = async(username:string, password:string)=>{
        if (username && password) {
            const hashedPassword = await hashPassword(password);
            const createUserResult/*  = result.rows */ = await this.knex("staff").insert({
                username:username,
                password:hashedPassword,
                created_at:new Date(),
                updated_at:new Date()
            }).returning("id");

            const createUserRowCount = createUserResult.length;
            console.log(createUserRowCount);
    
            if (createUserRowCount === 1) {
                return { msg: "Create User Success" };
            } else {
                return { msg: "Create User Fail" };
            }
        } else {
            return { msg: "Invalid input" };
        }
    }

    login = async(username:string, password:string)=>{ 
        const users = await this.knex("staff").select("*").where("username",username);
        if (users.length == 0) {
            return { success: false, error: "User 不存在" };
        }
        if (!checkPassword(password, users[0].password)) {
            return { success: false, error: "電郵或密碼錯誤" };
        }
        return { user: {
            username:users[0].username
        }};
    }

    loginGoogle = async (googleUserInfo:any) => {
        const users = (await this.knex.raw(`SELECT * FROM staff WHERE username = $1`, [googleUserInfo.email])).rows;
        let user = users[0];
        if (!user) {
            let hashedPassword = await hashPassword((Math.random() + 1).toString(36));
            const createUserResult = await this.knex.raw(`INSERT INTO staff (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING *`, [googleUserInfo.email, hashedPassword, "NOW()", "NOW()"]);
            const createUserRowCount = createUserResult.rowCount;
            user = createUserResult.rows[0];
            if (createUserRowCount != 1) {
                return { msg: "insert fail" };
            }
        }
        return user;
    }
}