import multer from "multer";

const singleStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        /* cb = callback */
        cb(null, "uploads");
    },
    filename: function (req, file, cb) {
        cb(null, `memo-image-${file.fieldname}.${file.mimetype.split("/")[1]}`);
    },
});

const multiStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        /* cb = callback */
        cb(null, "uploads");
    },
    filename: function (req, file, cb) {
        cb(null, `memo-image-${file.fieldname}.${file.mimetype.split("/")[1]}`);
    },
});

const singleUpload = multer({ storage: singleStorage });
const multiUpload = multer({ storage: multiStorage });

// req.file
export const multerFormSingle = singleUpload.single("image");
// req.files
export const multerFormMulti = multiUpload.array("images");
