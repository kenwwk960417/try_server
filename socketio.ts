import socketIO from "socket.io";
import { logger } from "./logger";

export let io: socketIO.Server;

export function setSocketIO(value: socketIO.Server) {
    io = value;
    io.on("connection", (socket) => {
        logger.info("[有Client 駁左socket] ", socket.id);
    });
}
