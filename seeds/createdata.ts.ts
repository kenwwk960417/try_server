import { Knex } from "knex";
import {hashPassword} from '../hash'
export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("staff").del();

    // Inserts seed entries
    await knex("staff").insert([
        { username:"admin@tecky.io", password: await hashPassword("123456") }
    ]);
    
    await knex("memos").del();
    await knex("memos").insert([
        {content:"Memo#1"},
        {content:"Memo#2"}
    ])
};
