import { UserController } from "../controllers/UserController";
import {UserService} from '../services/UserService';
import {Request,Response} from 'express'

jest.mock("express");

describe("Test Login Controller",()=>{
    let userController:UserController;
    let userService:UserService;

    beforeEach(()=>{
        userService = new UserService();
        userController = new UserController(userService);
    })

    test("Login Success",async()=>{
        let req = {
            body:{
                username:"admin@tecky.io",
                password:"123456"
            },
            session:{}
        } as any as Request;
        let res ={
            json:(data:any)=>data
        } as any as Response;

        const serviceSpy = jest.spyOn(userService,"login").mockImplementation(async(username,password)=>{
            return Promise.resolve({
                    user:{
                        username:"admin@tecky.io",
                        password:"123456"
                    }
                })
        });
        const resSpy = jest.spyOn(res,"json");
        await userController.login(req,res);
        
        expect(serviceSpy).toBeCalledTimes(1);
        expect(resSpy).toBeCalledWith({
            user:{
                username:"admin@tecky.io",
                password:"123456"
            }
        });
    });

    test("Login Fail : User doesn't exists",async()=>{
        let req = {
            body:{
                username:"admin@tecky.io",
                password:"123456"
            },
            session:{}
        } as any as Request;
        let res ={
            json:(data:any)=>data
        } as any as Response;

        const serviceSpy = jest.spyOn(userService,"login").mockImplementation(async(username,password)=>{
            return Promise.resolve({ success: false, error: "User 不存在" })
        });
        const resSpy = jest.spyOn(res,"json");
        await userController.login(req,res);
        expect(serviceSpy).toBeCalledTimes(1);
        expect(resSpy).toBeCalledWith({ success: false, error: "User 不存在" });
    })

    test("Login Fail : Wrong Password",async()=>{
        let req = {
            body:{
                username:"admin@tecky.io",
                password:"123456"
            },
            session:{}
        } as any as Request;
        let res ={
            json:(data:any)=>data
        } as any as Response;

        const serviceSpy = jest.spyOn(userService,"login").mockImplementation(async(username,password)=>{
            return Promise.resolve({ success: false, error: "電郵或密碼錯誤" })
        });
        const resSpy = jest.spyOn(res,"json");
        await userController.login(req,res);
        expect(serviceSpy).toBeCalledTimes(1);
        expect(resSpy).toBeCalledWith({ success: false, error: "電郵或密碼錯誤" });
    })

    /*
    test("Logout",()=>{})
    test("Get Current USer",()=>{})
    */
})