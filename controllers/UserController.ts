import {UserService} from '../services/UserService';
import { Request,Response } from 'express';

export class UserController{
    constructor(private userService:UserService){}
    register = async(req:Request,res:Response)=>{
        const { username, password } = req.body;
        const result = await this.userService.register(username, password);
        res.json(result);
    }
    getUser = async (req:Request,res:Response)=>{
        res.json({ user: req.session["user"] });
    }
    logout = async (req:Request,res:Response)=>{
        req.session.destroy((err) => {
            if (err) {
                return res.json({ success: false });
            }
            return res.json({ success: true });
        });
    }
    login = async (req:Request,res:Response)=>{
        const {username,password} = req.body;
        const result = await this.userService.login(username,password);
        if (result.user){
            req.session["user"] = result.user;
        }
        res.json(result);
    }

    loginGoogle = async (req:Request,res:Response)=>{
        const accessToken = req.session?.["grant"].response.access_token;
        const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
            method: "get",
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });
        const googleUserInfo = await fetchRes.json();
        const user:any= await this.userService.loginGoogle(googleUserInfo);
        if (user.msg){
            res.status(401).json(user);
        }
        if (req.session) {
            req.session["user"] = user;
        }
        return res.redirect("/admin/admin.html");
    }
}