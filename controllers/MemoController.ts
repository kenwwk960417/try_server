import {MemoService} from '../services/MemoService';
import { Request,Response } from 'express';
import { io } from "../socketio";

export class MemoController{
    constructor(private memoService:MemoService){}
    createMemo = async(req:Request,res:Response)=>{
        const { content } = req.body;
        const image : string|undefined= req.file?.filename;
        const result = await this.memoService.createMemo(content,image);
        if(!result.error){
            if (image){
                io.emit("new-memo", {
                    content,
                    image,
                });
            }else{
                io.emit("new-memo", {
                    content,
                });
            }
        }
        res.json(result);
    }

    updateMemoById= async(req:Request,res:Response)=>{
        const { id /*string!*/ } = req.params;
        const { content } = req.body;
        const image:string|undefined = req.file?.filename;
        const result = await this.memoService.updateMemoById(id,content,image);
        io.emit("update-memo-list");
        res.json(result);
    }

    getAllMemos = async(req:Request,res:Response)=>{
        const result = await this.memoService.getAllMemos();
        res.json(result);
    }

    deleteMemoById = async(req:Request,res:Response)=>{
        const { id } = req.params;
        const result = await this.memoService.deleteMemoById(id);
        io.emit("update-memo-list");
        res.json(result);
    }

    deleteAllMemo = async(req:Request,res:Response)=>{
        const result = await this.memoService.deleteAllMemo();
        res.json(result);
    }

    likeMemoById = async(req:Request,res:Response)=>{
        const { id } = req.params;
        let userId = req.session["user"].id;
        const result = await this.memoService.likeMemoById(id,userId);
        res.json(result);
    }

    getLikedMemosById = async(req:Request,res:Response)=>{
        const userId:any = req.query.user_id;
        const result = await this.memoService.getLikedMemosById(userId);
        res.json(result);
    }
}