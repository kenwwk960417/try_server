import express from "express";
import { isLoggedIn } from "../guard";
import { multerFormSingle } from "../multer";
import { memoController } from "../server";

const memoRoutes = express.Router();

memoRoutes.post("/memo", multerFormSingle, (req,res)=>memoController.createMemo(req,res));
memoRoutes.put("/memo/:id", multerFormSingle, (req,res)=>memoController.updateMemoById(req,res));
memoRoutes.get("/memos", (req,res)=>memoController.getAllMemos(req,res));
memoRoutes.delete("/memo/:id", (req,res)=>memoController.deleteMemoById(req,res));
memoRoutes.delete("/memos", (req,res)=>memoController.deleteAllMemo(req,res));
memoRoutes.post("/like/memo/:id", isLoggedIn, (req,res)=>memoController.likeMemoById(req,res));
memoRoutes.get("/admin/like_memos", isLoggedIn, (req,res)=>memoController.getLikedMemosById(req,res));

export default memoRoutes;
